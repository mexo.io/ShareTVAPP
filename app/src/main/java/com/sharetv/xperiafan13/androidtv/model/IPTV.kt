package com.sharetv.xperiafan13.androidtv.model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.Reader
import java.io.Serializable

data class IPTV(
        val title: String = "",
        val link: String = "",
        val img: String = ""
): Serializable {
    class Deserializer : ResponseDeserializable<IPTV> {
        override fun deserialize(reader: Reader) = Gson().fromJson(reader, IPTV::class.java)!!
    }

    class ListDeserializer : ResponseDeserializable<List<IPTV>> {
        override fun deserialize(reader: Reader): List<IPTV> {
            val type = object : TypeToken<List<IPTV>>() {}.type
            return Gson().fromJson(reader, type)
        }
    }
}