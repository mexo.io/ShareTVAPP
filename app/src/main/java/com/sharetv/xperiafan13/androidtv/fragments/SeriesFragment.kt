package com.sharetv.xperiafan13.androidtv.fragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.preference.PreferenceManager
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.util.Log
import android.view.*
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.webkit.WebViewClient
import com.github.kittinunf.fuel.Fuel
import com.jaredrummler.materialspinner.MaterialSpinner
import com.sharetv.xperiafan13.androidtv.R
import com.sharetv.xperiafan13.androidtv.utils.URLScrapper
import com.sharetv.xperiafan13.androidtv.views.ViewMovieActivity
import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebChromeClient
import com.google.gson.Gson
import com.sharetv.xperiafan13.androidtv.model.Response
import org.jetbrains.anko.support.v4.progressDialog


class SeriesFragment : Fragment() {

    private var webView: WebView? = null
    private var fab: FloatingActionButton? = null
    private var ringProgressDialog: ProgressDialog? = null
    private var dropdown: MaterialSpinner? = null
    private var url: String? = null
    private var catchedOpenLoad: String = ""
    private var catchedStream: String = ""
    private var catchedRapid: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.series_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webView = view.findViewById(R.id.webview)
        fab = view.findViewById<View>(R.id.fab) as FloatingActionButton
        dropdown = view.findViewById(R.id.spinner1)

        url = "http://fanpelis.com"
        setServers()
        setList()
        setFab()
    }

    private fun clearValues() {
        catchedOpenLoad = ""
        catchedStream = ""
        catchedRapid = ""
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setList() {
        clearValues()
        showLoadingDialog("Cargando página...")
        webView!!.loadUrl(url)
        val webSettings = webView!!.settings
        webSettings.javaScriptEnabled = true
        webView!!.settings.domStorageEnabled = true
        webView!!.settings.setSupportMultipleWindows(false)
        webView!!.webChromeClient = object : WebChromeClient() {}
        webView!!.addJavascriptInterface(this, "android")
        webView!!.webViewClient = object : WebViewClient() {
            @SuppressLint("NewApi")
            override fun shouldInterceptRequest(view: WebView, request: WebResourceRequest): WebResourceResponse? {
                if (url == "https://www.pelisyseries.tv/" || url == "https://canalpelis.com" || url == "http://www.tvpelis.tv/" || url == "http://frisons.fr" ||
                        url == "http://peliculasmx.net/" || url == "http://maxipelis24.com/" || url == "http://pelisplus.co/" || url == "http://simpsons-latino.com/") {
                    val tempLink = request.requestHeaders.toString()
                    if (tempLink.contains("openload") || tempLink.contains("streamango") || tempLink.contains("rapidvideo")) {
                        val temp = tempLink.split("Referer=")[1]
                        if (!catchedOpenLoad.contains(temp) || !catchedStream.contains(temp) || !catchedRapid.contains(temp)) {
                            setOpenLoad(temp)
                        }
                    }
                }
                return null
            }

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                closeDialog()
            }
        }

        webView!!.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            clearValues()
            if (keyCode == KeyEvent.KEYCODE_BACK
                    && event.action == MotionEvent.ACTION_UP
                    && webView!!.canGoBack()) {
                handler.sendEmptyMessage(1)
                return@OnKeyListener true
            }
            false
        })
    }

    private fun splitComma(data: String): String {
        var tempMail = data
        if (data.contains(",")) {
            tempMail = data.split(",")[0]
        }
        return tempMail
    }

    private fun setOpenLoad(data: String) {
        when {
            data.contains("openload") -> catchedOpenLoad = data.split("}")[0]
            data.contains("streamango") -> catchedStream = data.split("}")[0]
            data.contains("rapidvideo") -> {
                catchedRapid = splitComma(data.split("}")[0])
            }
        }
    }

    private fun setServers() {
        dropdown!!.setItems("Fanpelis", "Cinecalidad", "PelisySeries", "Wikiseriesypelis", "Serieslan", "CanalPelis", "TVPelis", "PeliculasMX", "MaxiPelis", "PelisPlus", "Frisons", "Simpsons")
        dropdown!!.setOnItemSelectedListener { _, position, _, _ ->
            when (position) {
                0 -> url = "http://fanpelis.com"
                1 -> url = "https://www.cinecalidad.to/"
                2 -> url = "https://www.pelisyseries.tv/"
                3 -> url = "https://wikiseriesypelis.com/"
                4 -> url = "https://serieslan.com/"
                5 -> url = "https://canalpelis.com"
                6 -> url = "http://www.tvpelis.tv/"
                7 -> url = "http://peliculasmx.net/"
                8 -> url = "http://maxipelis24.com/"
                9 -> url = "http://pelisplus.co/"
                10 -> url = "http://frisons.fr"
                11 -> url = "http://simpsons-latino.com/"
            }
            setList()
        }
    }


    private fun setFab() {
        fab!!.setOnClickListener { _ ->
            when (url) {
                "https://www.pelisyseries.tv/" -> setRequestNeworkServers()
                "http://fanpelis.com" -> {
                    webView!!.loadUrl("""
                    javascript:(function() {
                    var links = [];
                        var list = document.getElementsByTagName("iframe");
                        for(var i = 0; i < list.length; i++) {
                            if (list.item(i).src.includes("rapidvideo") || list.item(i).src.includes("openload")) {
                                links.push(list.item(i).src)
                            }
                        }
                        android.onData(links.toString());
                    })()
                """.trimIndent())
                }
                "https://www.cinecalidad.to/" -> {
                    webView!!.loadUrl("""
                    javascript:(function() {
                    var links = [];
                        ${'$'}('a[class="link onlinelink"]').each(function(index,item){
                            var service = item.getAttribute('service');
                            if(service == "OnlineOpenload" || service == "OnlineStreamango" || service == "OnlineRapidVideo") {
                                 links.push(item.getAttribute('data') + "{}" +  service)
                            }
                        });
                        android.onData(links.toString());
                    })()
                """.trimIndent())
                }
                "https://wikiseriesypelis.com/" -> {
                    webView!!.loadUrl("""
                    javascript:(function() {
                    var links = [];
                        var list = document.getElementsByTagName("iframe");
                        for(var i = 0; i < list.length; i++) {
                            if (list.item(i).src.includes("cine24")) {
                                links.push(list.item(i).src)
                            }
                        }
                        android.onData(links.toString());
                    })()
                """.trimIndent())
                }
                "https://serieslan.com/" -> {
                    webView!!.loadUrl("""
                    javascript:(function() {
                        var button = document.getElementsByClassName("selop")[0];
                        if (button) {
                            button.click();
                            android.onData( document.getElementById("eb").src);
                        } else {
                            var button = document.getElementsByClassName("play-bt")[0];
                            if (button) {
                                button.click();
                                var list = document.getElementsByTagName("iframe");
                                for(var i = 0; i < list.length; i++) {
                                    if (list.item(i).src.includes("viteca")) {
                                        links.push(list.item(i).src)
                                    }
                                }
                                android.onData(links.toString());
                            }
                        }
                    })()
                """.trimIndent())
                }
                "https://canalpelis.com" -> setRequestNeworkServers()
                "http://www.tvpelis.tv/" -> setRequestNeworkServers()
                "http://peliculasmx.net/" -> setRequestNeworkServers()
                "http://maxipelis24.com/" -> setRequestNeworkServers()
                "http://pelisplus.co/" -> setRequestNeworkServers()
                "http://frisons.fr" -> setRequestNeworkServers()
                "http://simpsons-latino.com/" -> setRequestNeworkServers()
            }
        }
    }

    private fun setRequestNeworkServers() {
        showLoadingDialog("Get video links ...")
        Handler().postDelayed({
            if (catchedOpenLoad != "" || catchedStream != "" || catchedRapid != "") {
                formatTVPelis()
            } else {
                showDialog()
            }
        }, 7000)
    }

    private fun showLoadingDialog(msg: String) {
        ringProgressDialog = ProgressDialog(context, R.style.AlertDialogCustom)
        ringProgressDialog!!.setTitle("Working ...")
        ringProgressDialog!!.setMessage(msg)
        ringProgressDialog!!.isIndeterminate
        ringProgressDialog!!.show()
    }

    private fun closeDialog() {
        ringProgressDialog!!.dismiss()
    }

    private val handler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(message: Message) {
            when (message.what) {
                1 -> {
                    webViewGoBack()
                }
            }
        }
    }

    private fun webViewGoBack() {
        webView!!.goBack()
    }

    private fun showDialog() {
        closeDialog()
        val alertDialog = AlertDialog.Builder(ContextThemeWrapper(context, R.style.AlertDialogCustom)).create()
        alertDialog.setTitle("Alert")
        alertDialog.setMessage("Invalid link")
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK"
        ) { dialog, _ -> dialog.dismiss() }
        alertDialog.show()
    }

    private fun returnIDOpenLoad(number: String): String {
        val data = number.split("\\s".toRegex())
        var id = ""
        for (char: String in data) {
            id += ((char.toInt() - 4).toChar())
        }
        return "https://openload.co/embed/$id"
    }

    private fun returnIDStreamMango(number: String): String {
        val data = number.split("\\s".toRegex())
        var id = ""
        for (char: String in data) {
            id += ((char.toInt() - 4).toChar())
        }
        return "https://streamango.com/embed/$id"
    }

    private fun returnIDRapidVideo(number: String): String {
        val data = number.split("\\s".toRegex())
        var id = ""
        for (char: String in data) {
            id += ((char.toInt() - 4).toChar())
        }
        return "https://www.rapidvideo.com/embed/$id"
    }

    private fun formatTVPelis() {
        closeDialog()
        val builder = AlertDialog.Builder(ContextThemeWrapper(context, R.style.AlertDialogCustom))
        builder.setTitle("Choose an server")
        val servers: ArrayList<String> = ArrayList()
        val links: ArrayList<String> = ArrayList()
        if (catchedOpenLoad != "") {
            servers.add("Openload")
            links.add(catchedOpenLoad)
        }
        if (catchedStream != "") {
            servers.add("Streamango")
            links.add(catchedStream)
        }
        if (catchedRapid != "") {
            servers.add("RapidVideo")
            links.add(catchedRapid)
        }
        builder.setItems(servers.toTypedArray()) { _, which ->
            when (which) {
                0 -> {
                    showLoadingDialog("Get video link ...")
                    playURL(links[which])
                }
                1 -> {
                    showLoadingDialog("Get video link ...")
                    playURL(links[which])
                }
                2 -> {
                    showLoadingDialog("Get video link ...")
                    playURL(links[which])
                }
            }
        }
        val dialog = builder.create()
        dialog.show()
    }

    private fun formatCineClaidad(data: String) {
        val builder = AlertDialog.Builder(ContextThemeWrapper(context, R.style.AlertDialogCustom))
        builder.setTitle("Choose an server")
        val links: ArrayList<String> = ArrayList()
        val servers: ArrayList<String> = ArrayList()
        val dataComplete: List<String> = data.split(",").map { it.trim() }
        for (server: String in dataComplete) {
            val new = server.split("{}")
            when (new[1]) {
                "OnlineOpenload" -> {
                    links.add(returnIDOpenLoad(new[0]))
                    servers.add("Openload")
                }
                "OnlineStreamango" -> {
                    links.add(returnIDStreamMango(new[0]))
                    servers.add("Streamango")
                }
                "OnlineRapidVideo" -> {
                    links.add(returnIDRapidVideo(new[0]))
                    servers.add("Rapidvideo")
                }
            }
        }
        builder.setItems(servers.toTypedArray()) { _, which ->
            when (which) {
                0 -> playURL(links[0])
                1 -> playURL(links[1])
                2 -> playURL(links[2])
            }
            showLoadingDialog("Get video link ...")
        }
        val dialog = builder.create()
        dialog.show()
    }


    private fun showListSelection(data: String) {
        val builder = AlertDialog.Builder(ContextThemeWrapper(context, R.style.AlertDialogCustom))
        builder.setTitle("Choose an server")
        val links: List<String> = data.split(",").map { it.trim() }
        val servers: ArrayList<String> = ArrayList()
        for (a: String in links) {
            when {
                a.contains("rapidvideo") -> servers.add("RapidVideo")
                a.contains("openload") || a.contains("oload") -> servers.add("Openload")
                a.contains("cine24") -> servers.add("Google Drive")
                a.contains("viteca") -> servers.add("Google Drive")
                a.contains("pelisyseries") -> servers.add("Openload")
            }
        }

        builder.setItems(servers.toTypedArray()) { _, which ->
            when (which) {
                0 -> playURL(links[0])
                1 -> playURL(links[1])
                2 -> playURL(links[2])
                3 -> playURL(links[3])
            }
            showLoadingDialog("Get video link ...")
        }
        val dialog = builder.create()
        dialog.show()
    }

    private fun showTV(url: String) {
        closeDialog()
        val builder = AlertDialog.Builder(ContextThemeWrapper(context, R.style.AlertDialogCustom))
        builder.setTitle("Choose an option")
        val animals = arrayOf(getString(R.string.tv_box), getString(R.string.local))
        builder.setItems(animals) { _, which ->
            when (which) {
                0 -> postData(url)
                1 -> {
                    val prefs = PreferenceManager.getDefaultSharedPreferences(context)
                    if (prefs.getBoolean("pref_external_player", false)) {
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.setDataAndType(Uri.parse(url), "video/*")
                        startActivity(intent)
                    } else {
                        val intent = Intent(context, ViewMovieActivity::class.java)
                        intent.putExtra("data", url)
                        startActivity(intent)
                    }
                }
            }
        }
        val dialog = builder.create()
        dialog.show()
    }

    private fun reloadLink(value: String) {
        activity!!.runOnUiThread {
            val action = URLScrapper(value, context!!, this)
            action.returnData()
        }
    }

    private fun playURL(value: String) {
        Log.i("RESULTADO", value)
        reloadLink(value)
    }

    fun load(data: String) {
        if (data == "none" || data == "") {
            showDialog()
        } else {
            if (data.contains("https://openload.co/embed/") || data.contains("https://streamango.com/embed/")) {
                reloadLink(data)
            } else {
                showTV(data)
            }
        }
    }

    @JavascriptInterface
    fun onData(value: String) {
        Log.i("DATAVALUE", value)
        if (url == "https://www.cinecalidad.to/") {
            formatCineClaidad(value)
        } else {
            showListSelection(value)
        }
    }

    private fun postData(key: String) {
        var code = PreferenceManager.getDefaultSharedPreferences(context).getString("code", "none")
        if (code == "none") {
            code = PreferenceManager.getDefaultSharedPreferences(context).getString("pref_ip", "none")
        }
        if (code == "none") {
            showError()
        } else {
            Fuel.post("http://$code:5050/".replace("\\s".toRegex(), ""))
                    .body(Gson().toJson(Response(torrent = false, movie = true , title = "ShareTV", key = key, img = "none")))
                    .response { _, response, _ ->
                        Log.i("Response", response.toString())
                    }
        }
    }

    private fun showError() {
        val alertDialog = AlertDialog.Builder(ContextThemeWrapper(context, R.style.AlertDialogCustom)).create()
        alertDialog.setTitle("Alert")
        alertDialog.setMessage("Sin IP registrada")
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK"
        ) { dialog, _ -> dialog.dismiss() }
        alertDialog.show()
    }
}