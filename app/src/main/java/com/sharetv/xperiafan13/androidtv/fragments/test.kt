package com.sharetv.xperiafan13.androidtv.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.just.agentweb.AgentWeb
import com.sharetv.xperiafan13.androidtv.R
import android.net.http.SslError
import android.util.Log
import android.webkit.*


class test : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.test, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        val webClient = object : WebViewClient() {
            @SuppressLint("NewApi")
            override fun shouldInterceptRequest(view: WebView, request: WebResourceRequest): WebResourceResponse? {
                val tempLink = request.url.toString()
                Log.i("RRR", tempLink)
                return null
            }


            override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
                handler.proceed()
            }
        }

        val url = "http://cablegratis.tv/canal-en-vivo/de-pelicula"
        var mAgentWeb = AgentWeb.with(this)
                .setAgentWebParent(view as LinearLayout, LinearLayout.LayoutParams(-1, -1))
                .useDefaultIndicator()
                .setWebViewClient(webClient)
                .setWebChromeClient (WebChromeClient())
                .createAgentWeb()
                .ready()
                .go(url)
    }

}