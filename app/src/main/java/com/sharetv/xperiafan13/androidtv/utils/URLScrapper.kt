package com.sharetv.xperiafan13.androidtv.utils

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.Handler
import android.support.annotation.RequiresApi
import android.view.View
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.webkit.WebViewClient
import com.sharetv.xperiafan13.androidtv.fragments.SeriesFragment


class URLScrapper(private var url: String, private var ctx: Context, private var f: SeriesFragment) {

    private var serverType: String = "none"
    private var returnURL: String = "none"
    private var webView = WebView(ctx)


    @SuppressLint("SetJavaScriptEnabled")
    fun returnData() {
        webView.visibility = View.INVISIBLE
        webView.loadUrl(returnDirectLink(url))
        val webSettings = webView.settings
        webSettings.javaScriptEnabled = true
        webView.settings.setSupportMultipleWindows(false)
        webView.addJavascriptInterface(this, "android")
        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                when (serverType) {
                    "OpenLoad" -> webView.loadUrl(returnOpenload())
                    "Streamango" -> webView.loadUrl(returnStreamango())
                    "RapidVideo" -> webView.loadUrl(returnRapidVideo())
                    "VeVi" -> webView.loadUrl(returnTheVideo())
                    "Cine24" -> webView.loadUrl(returnCine24())
                    "Viteca" -> webView.loadUrl(returnViteca())
                    "Pelis" -> webView.loadUrl(returnPelis())
                }
                    if (serverType == "RapidVideo") {
                        Handler().postDelayed({
                            webView.loadUrl(returnRapidVide())
                        }, 5000)
                    }
            }
        }
    }

    private fun setType() {
        when {
            url.contains("openload") || url.contains("oload") -> {
                serverType = "OpenLoad"
            }
            url.contains("streamango") -> {
                serverType = "Streamango"
            }
            url.contains("rapidvideo") -> {
                serverType = "RapidVideo"
            }
            url.contains("vev.io/") -> {
                serverType = "VeVi"
            }
            url.contains("cine24") -> {
                serverType = "Cine24"
            }
            url.contains("viteca") -> {
                serverType = "Viteca"
            }
            url.contains("player.pelisyseries") -> {
                serverType = "Pelis"
            }
            else -> url
        }
    }

    private fun returnDirectLink(url: String): String {
        setType()
        return when {
            url.contains("https://openload.co/f/") -> {
                url.replace("/f/", "/embed/")
            }
            url.contains("https://streamango.com/f/") -> {
                url.replace("/f/", "/embed/")
            }
            url.contains("https://www.rapidvideo.com/v/") -> {
                url.replace("/v/", "/embed/")
            }
            url.contains("https://vev.io/") -> {
                val data = url.split("io/")
                data[0] + "io/embed/" + data[1]
            }
            else -> url
        }
    }

    private fun callMethod() {
        f.load(returnURL)
    }

    private fun returnOpenload(): String = """
        javascript:(function() {
            var data = document.getElementById("DtsBlkVFQx");
            if (data) {
                android.onData(data.textContent);
            } else {
                android.onData("none");
            }
        })()
    """.trimIndent()

    private fun returnStreamango(): String = """
        javascript:(function() {
            var data = document.getElementById("mgvideo_html5_api");
            if (data) {
                android.onData(data.src);
            } else {
                android.onData("none");
            }
        })()
    """.trimIndent()

    private fun returnRapidVideo(): String {
        return  """
        javascript:(function() {
        document.getElementsByName("confirm")[0].click();
        })()
    """.trimIndent()
    }

    private fun returnRapidVide(): String {
        return  """
        javascript:(function() {
            var data = document.getElementById("videojs_html5_api");
                if (data) {
                    android.onData(data.src);
                } else {
                    android.onData("none");
                }
        })()
    """.trimIndent()
    }

    private fun returnTheVideo(): String = """
        javascript:(function() {
            var data = document.getElementsByTagName("video")[0];
            if (data) {
                android.onData(data.src);
            } else {
                android.onData("none");
            }
        })()
    """.trimIndent()

    private fun returnCine24(): String = """
        javascript:(function() {
            var data = document.getElementById("video_html5_api");
            if (data) {
                android.onData(data.src);
            } else {
                android.onData("none");
            }
        })()
    """.trimIndent()

    private fun returnViteca(): String = """
        javascript:(function() {
        var link = document.getElementById("video_html5_api");
        if (link) {
            android.onData(link.src);
        } else {
            var list = document.getElementsByTagName("iframe");
            var link = "";
            for(var i = 0; i < list.length; i++) {
                if (list.item(i).src.includes("openload")) {
                    link = list.item(i).src;
                }
            }
            android.onData(link);
        }
        })()
    """.trimIndent()

    private fun returnPelis(): String = """
        javascript:(function() {
            var scripts = document.getElementsByTagName("script");
            for (var i=0;i<scripts.length;i++) {
                var link = scripts[i].innerHTML;
                if (link.includes("streamango")) {
                    var n = link.search("https://streamango.com/embed/");
                    var a = link.slice(n).split("'")[0];
                    android.onData(a);
                    return
                } else if (link.includes("openload")) {
                    var n = link.search("https://openload.co");
                    var a = link.slice(n).split("'")[0];
                    android.onData(a);
                    return
                }
            }
        })()
    """.trimIndent()

    @JavascriptInterface
    fun onData(value: String) {
        if (value == "none") {
            callMethod()
        } else {
            getLink(value)
        }
    }


    private fun getLink(value: String) {
        when (serverType) {
            "OpenLoad" -> returnURL = ("https://openload.co/stream/$value")
            "Streamango" -> returnURL = (value)
            "RapidVideo" -> returnURL = (value)
            "VeVi" -> returnURL = (value)
            "Cine24" -> returnURL = (value)
            "Viteca" -> returnURL = (value)
            "Pelis" -> returnURL = (value)
        }
        callMethod()
    }

}