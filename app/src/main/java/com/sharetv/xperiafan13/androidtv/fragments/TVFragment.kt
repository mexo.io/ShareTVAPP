package com.sharetv.xperiafan13.androidtv.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.sharetv.xperiafan13.androidtv.R
import com.sharetv.xperiafan13.androidtv.adapter.NumberedAdapter
import com.sharetv.xperiafan13.androidtv.model.TV

class TVFragment : Fragment() {

    private var recyclerView: RecyclerView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.tv_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.recycler_view)
        recyclerView!!.layoutManager = GridLayoutManager(context, calculateNoOfColumns(context!!))
        httpListResponseObject()
    }

    private fun calculateNoOfColumns(context: Context): Int {
        val displayMetrics = context.resources.displayMetrics
        val dpWidth = displayMetrics.widthPixels / displayMetrics.density
        return (dpWidth / 150).toInt()
    }

    private fun httpListResponseObject() {
        "http://173.255.196.18:2828/tv/".httpGet()
                .responseObject(TV.ListDeserializer()) { _, _, result ->
                    update(result)
                }
    }

    private fun update(result: Result<List<TV>, FuelError>) {
        result.fold(success = {
            recyclerView!!.adapter = NumberedAdapter(it, context!!)
        }, failure = {
            Log.e("Error", String(it.errorData))
        })
    }

}