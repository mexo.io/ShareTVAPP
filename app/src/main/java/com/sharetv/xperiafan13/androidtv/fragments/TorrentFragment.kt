package com.sharetv.xperiafan13.androidtv.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import com.github.se_bastiaan.torrentstream.utils.ThreadUtils.runOnUiThread
import com.jaredrummler.materialspinner.MaterialSpinner
import com.sharetv.xperiafan13.androidtv.R
import com.sharetv.xperiafan13.androidtv.adapter.TorrentAdapter
import com.sharetv.xperiafan13.androidtv.model.Torrent

class TorrentFragment : Fragment() {

    private var recyclerView: RecyclerView? = null
    private var dropdown: MaterialSpinner? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.torrent_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.recycler_view)
        recyclerView!!.setHasFixedSize(true)
        recyclerView!!.layoutManager = LinearLayoutManager(context)
        dropdown = view.findViewById(R.id.spinner1)
        httpListResponseObject()
        setServers()
    }

    private fun setServers() {
        val genres = arrayOf("Todas","Drama", "Familia", "Música", "Comedia", "Romance",
                "Suspense", "Terror", "Estrenos", "Animación", "Western", "Fantasía", "Historia", "Crimen", "Acción")
        dropdown!!.setItems("Todas", "Drama", "Familia", "Música", "Comedia", "Romance",
                "Suspense", "Terror", "Estrenos", "Animación", "Western", "Fantasía", "Historia", "Crimen", "Acción")
        dropdown!!.setOnItemSelectedListener { _, position, _, _ ->
            if (genres[position] == "Todas") {
                httpListResponseObject()
            } else {
                httpListGenres(genres[position])
            }
        }
    }

    private fun httpListGenres(data: String) {
        "http://173.255.196.18:2828/torrent/genre/$data".httpGet().responseObject(Torrent.ListDeserializer()) { _, _, result ->
            update(result)
        }
    }

    private fun httpListResponseObject() {
        "http://173.255.196.18:2828/torrent/".httpGet()
                .responseObject(Torrent.ListDeserializer()) { _, _, result ->
                    update(result)
                }
    }

    private fun update(result: Result<List<Torrent>, FuelError>) {
        result.fold(success = {
            recyclerView!!.adapter = TorrentAdapter(it, context!!)
            runOnUiThread{
                recyclerView!!.adapter!!.notifyDataSetChanged()
            }
        }, failure = {
            Log.e("Error", String(it.errorData))
        })
    }



}