package com.sharetv.xperiafan13.androidtv.model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.Reader
import java.io.Serializable

data class TV(
        val title: String = "",
        val key: String = "",
        val img: String = "",
        val list: String = "",
        val mx: Boolean = false,
        val description: String = ""
): Serializable {
    class Deserializer : ResponseDeserializable<TV> {
        override fun deserialize(reader: Reader) = Gson().fromJson(reader, TV::class.java)
    }

    class ListDeserializer : ResponseDeserializable<List<TV>> {
        override fun deserialize(reader: Reader): List<TV> {
            val type = object : TypeToken<List<TV>>() {}.type
            return Gson().fromJson(reader, type)
        }
    }
}