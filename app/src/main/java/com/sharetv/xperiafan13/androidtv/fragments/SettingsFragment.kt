package com.sharetv.xperiafan13.androidtv.fragments

import android.content.SharedPreferences
import android.os.Bundle
import com.sharetv.xperiafan13.androidtv.R
import android.support.v7.preference.PreferenceFragmentCompat
import android.util.Log


class SettingsFragment : PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener {
    override fun onSharedPreferenceChanged(pref: SharedPreferences?, key: String?) {
       when(key) {
           "pref_external_player" -> {

           }
       }
    }

    override fun onCreatePreferences(p0: Bundle?, p1: String?) {
        activity!!.setTheme (R.style.SettingStyle)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity!!.setTheme (R.style.SettingStyle)
        addPreferencesFromResource(R.xml.preferences)
    }

    override fun onResume() {
        super.onResume()
        preferenceManager.sharedPreferences.registerOnSharedPreferenceChangeListener(this)

    }

    override fun onPause() {
        preferenceManager.sharedPreferences.unregisterOnSharedPreferenceChangeListener(this)
        super.onPause()
    }
}